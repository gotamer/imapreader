[![GoDoc](https://godoc.org/bitbucket.org/gotamer/imapreader?status.svg)](http://godoc.org/bitbucket.org/gotamer/imapreader)

Simple interface for reading IMAP emails in Golang
